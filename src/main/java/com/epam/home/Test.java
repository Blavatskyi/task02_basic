package com.epam.home;

import java.util.*;

/**
 * The Test program implements list of tasks which have been given for homework.
 *
 * @author Roman Blavatskyi
 * @version 1.0
 * @since 2019-11-10
 */
public class Test {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter first number of the interval: ");
        int intervalNumber1 = scanner.nextInt();
        scanner.nextLine();

        System.out.println("Enter second number of the interval: ");
        int intervalNumber2 = scanner.nextInt();
        scanner.nextLine();

        System.out.println("Your interval - [" + intervalNumber1 + ", "
                + intervalNumber2 + "]");

        List<Integer> oddNumbersList =
                getOddNumbers(intervalNumber1, intervalNumber2);
        List<Integer> evenNumbersList =
                getEvenNumbers(intervalNumber1, intervalNumber2);

        System.out.println("Odd numbers: " + oddNumbersList);
        System.out.println("Even numbers: " + evenNumbersList);

        int sum = getSum(oddNumbersList, evenNumbersList);
        System.out.println("Sum of Odd and Even numbers: " + sum);

        int theBiggestOdd = findTheBiggestOddNumber(oddNumbersList);
        System.out.println("The biggest odd number: " + theBiggestOdd);

        int theBiggestEven = findTheBiggestEvenNumber(evenNumbersList);
        System.out.println("The biggest even number: " + theBiggestEven);

        getFibonacciSequence(theBiggestOdd, theBiggestEven);


    }

    /**
     * This method is used to find and get all odd numbers from the interval
     *
     * @param firstNumber  This is the beginning of the interval
     * @param secondNumber This is the end of the interval
     * @return List This returns list of odd numbers
     */
    private static List getOddNumbers(int firstNumber, int secondNumber) {
        List<Integer> oddNumbersList = new ArrayList<>();
        for (int i = firstNumber; i <= secondNumber; i++) {
            if (i % 2 != 0) {
                oddNumbersList.add(i);
            }
        }
        return oddNumbersList;
    }

    /**
     * This method is used to find and get all even numbers from the interval
     *
     * @param firstNumber  This is the beginning of the interval
     * @param secondNumber This is the end of the interval
     * @return List This returns list of even numbers
     */
    private static List getEvenNumbers(int firstNumber, int secondNumber) {
        List<Integer> evenNumbersList = new ArrayList<>();
        for (int i = secondNumber; i >= firstNumber; i--) {
            if (i % 2 == 0) {
                evenNumbersList.add(i);
            }
        }
        return evenNumbersList;
    }

    /**
     * This method is used to sum even and odd numbers
     *
     * @param oddNumbersList  This is a list with odd numbers
     * @param evenNumbersList This is a list with even numbers
     * @return int This returns sum of odd and even numbers
     */
    private static int getSum(List oddNumbersList, List evenNumbersList) {
        int oddSum = 0;
        int evenSum = 0;
        Iterator iterator1 = oddNumbersList.iterator();
        Iterator iterator2 = evenNumbersList.iterator();
        while (iterator1.hasNext()) {
            oddSum += (int) iterator1.next();
        }

        while (iterator2.hasNext()) {
            evenSum += (int) iterator2.next();
        }

        return oddSum + evenSum;
    }

    /**
     * This method is used to find the biggest odd number from the interval
     *
     * @param oddNumbers This is a list of odd numbers
     * @return int This returns the biggest odd number
     */
    private static int findTheBiggestOddNumber(List oddNumbers) {
        Collections.sort(oddNumbers);
        return (int) oddNumbers.get(oddNumbers.size() - 1);
    }

    /**
     * This method is used to find the biggest even number from the interval
     *
     * @param evenNumbers This is a list of even numbers
     * @return int This returns the biggest even number
     */
    private static int findTheBiggestEvenNumber(List evenNumbers) {
        Collections.sort(evenNumbers);
        return (int) evenNumbers.get(evenNumbers.size() - 1);
    }

    /**
     * This method is used to get Fibonacci sequence of given size
     * and calculate percentage of odd & even numbers in the sequence
     *
     * @param n1 This is the biggest odd number(first number of the sequence)
     * @param n2 This is the biggest even number(second number of the sequence)
     */
    private static void getFibonacciSequence(int n1, int n2) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter size of set: ");
        int sizeOfSet = scanner.nextInt();
        scanner.nextLine();
        int n3;
        int oddCount = 0;
        int evenCount = 0;
        if (n1 % 2 != 0) {
            oddCount++;
        } else {
            evenCount++;
        }
        System.out.print(n1 + " " + n2);

        for (int i = 2; i < sizeOfSet; i++) {
            n3 = n2 + n1;
            System.out.print(" " + n3);
            if (n3 % 2 != 0) {
                oddCount++;
            } else {
                evenCount++;
            }
            n1 = n2;
            n2 = n3;
        }

        System.out.println();

        int percentage0fOddNumbers = (100 * oddCount) / sizeOfSet;
        int percentage0fEvenNumbers = 100 - percentage0fOddNumbers;

        System.out.println("Percentage of odd numbers in the Fibonacci set: "
                + percentage0fOddNumbers + "%");

        System.out.println("Percentage of even numbers in the Fibonacci set: "
                + percentage0fEvenNumbers + "%");
    }


}
